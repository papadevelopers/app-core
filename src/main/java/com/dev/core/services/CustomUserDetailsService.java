package com.dev.core.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dev.core.entities.User;
import com.dev.core.repositories.UserDetailsRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private final UserDetailsRepository userDetailsRepository;

	public CustomUserDetailsService(UserDetailsRepository userDetailsRepository) {
		this.userDetailsRepository = userDetailsRepository;
	}
	
	/**
	 * Load a user with username or email address.
	 */
	@Override
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		// Let people login with either username or email
		UserDetails user = userDetailsRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail).orElseThrow(
				() -> new UsernameNotFoundException("User not found with username or email : " + usernameOrEmail));

		return user;
	}

	@Transactional
	public UserDetails save(User user) {
		return this.userDetailsRepository.save(user);
	}

	public boolean existByUsername(String usernameOrEmail) {
		Optional<User> optionalUser = this.userDetailsRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
		return optionalUser.isPresent();
	}
}
