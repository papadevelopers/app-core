package com.dev.core.constants;


public enum ROLES {

	USER ("ROLE_USER"),
	ADMIN ("ROLE_ADMIN");
	
	public String value;
	
	private ROLES(String value) {
		this.value = value;
	}
}
