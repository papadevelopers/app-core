package com.dev.core.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public final class AppConstants {
	public static final String PUBLIC_API = "/api/public/v1";
	public static final String PRIVATE_API = "/api/v1";
	
	public static long JWT_TOKENEXP;
	public static String JWT_SECRETKEY;
	
	public AppConstants(@Value("${jwt.token_expiry}") long JWT_TOKENEXP , 
			@Value("${jwt.secret_key}") String JWT_SECRETKEY) {
		AppConstants.JWT_TOKENEXP = JWT_TOKENEXP;
		AppConstants.JWT_SECRETKEY = JWT_SECRETKEY;
	}
}
