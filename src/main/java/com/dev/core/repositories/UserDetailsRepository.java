package com.dev.core.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dev.core.entities.User;

@Repository
public interface UserDetailsRepository extends JpaRepository<User, String> {
	public Optional<User> findByUsernameOrEmail(String username, String email);
	
	@Transactional
	@Modifying
	@Query("delete from User u where u.username = ?1 or u.email = ?2")
	public void removeByUsernameOrEmail(String username , String email);
}
