package com.dev.core.entities;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.proxy.HibernateProxyHelper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;

/*According to spring security there must a table named
'user' and user should have list of authority it can be one to many
or many to many. User should implements UserDetails and will be stored
as a UserPrinciple in Sprinng Security context which is shared 
among all the threads.*/
@Entity
@Table(name = "users")
@Data
public class User implements UserDetails, Cloneable, Comparable<String> {
	private static final long serialVersionUID = 1L;
	@Column(nullable = false, unique = true)
	private String username;

	@Column(nullable = false)
	/* write only to not transfer this field to frontend */
	private String password;

	@Column
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id;

	private String email;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USERS_AUTHORITIES", joinColumns = @JoinColumn(name = "USERS_ID", referencedColumnName = "ID"))
	@Cascade(CascadeType.ALL)
	private Collection<Authority> authorities = new ArrayList<>();

	@Column @Transient private String token;

	@Column
	private Boolean enabled;

	@Column
	private boolean credentialsNonExpired;

	@Column
	private boolean accountNonExpired;

	@Column
	private boolean accountNonLocked;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	/*
	 * Generic implementation of equal method to be used by all entities.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		if (getUsername() == null || ((User) obj).getUsername() == null) {
			return false;
		}
		if (!getUsername().equals(((User) obj).getUsername())) {
			return false;
		}
		if (!HibernateProxyHelper.getClassWithoutInitializingProxy(obj).isAssignableFrom(this.getClass())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return getUsername() == null ? super.hashCode() : getUsername().hashCode();
	}

	/**
	 * Global comparable. User may use instance of to implement comparisons for
	 * different class. Defaults: Strings will be compared lexicological.
	 */
	@Override
	public int compareTo(String o) {
		return ((String) this.getUsername()).compareTo((String) o);
	}
}
