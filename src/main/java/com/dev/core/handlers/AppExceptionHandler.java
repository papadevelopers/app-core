package com.dev.core.handlers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class AppExceptionHandler {

	@ExceptionHandler(UsernameNotFoundException.class)
	public ResponseEntity<String> handleUserNotFound(UsernameNotFoundException exception){
		log.error(new String(""+exception.getStackTrace()));
		return ResponseEntity.badRequest().body(exception.getMessage());
	}
	
}
