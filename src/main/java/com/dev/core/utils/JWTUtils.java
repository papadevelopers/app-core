package com.dev.core.utils;

import java.util.Calendar;
import java.util.Date;

import org.springframework.security.core.userdetails.UserDetails;

import com.dev.core.constants.AppConstants;
import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;

/**
 * Utils class for token management.
 * @author p-kumar
 *
 */
@Slf4j
public final class JWTUtils {

	private static final Gson gson = new Gson();
	
	public static final String getUserIdFromJwt(String token) {
		Claims claims = Jwts.parser()
				.setSigningKey(AppConstants.JWT_SECRETKEY)
				.parseClaimsJws(token)
				.getBody();
		
		return String.valueOf(claims.getSubject());
	}
	
	 public static final String generateToken(UserDetails user) {
	        Date now = Calendar.getInstance().getTime();
	        //constants.JWT_TOKENEXP
	        Date expiryDate = new Date(now.getTime() + (60*60*60*7));

	        return Jwts.builder()
	                .setSubject(gson.toJson(user).toString())
	                .setIssuedAt(now)
	                .setExpiration(expiryDate)
	                .signWith(SignatureAlgorithm.HS512, AppConstants.JWT_SECRETKEY)
	                .compact();
	    }
	 
	 public static final boolean validateToken(String authToken) throws JwtException{
	        try {
	            Jwts.parser().setSigningKey(AppConstants.JWT_SECRETKEY)
	            	.parseClaimsJws(authToken);
	            return true;
	        } catch (MalformedJwtException | ExpiredJwtException 
	        		| UnsupportedJwtException  | IllegalArgumentException ex) {
	            log.error(ex.getLocalizedMessage());
	            throw ex;
	        }
	 }
}
