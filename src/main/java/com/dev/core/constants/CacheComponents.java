package com.dev.core.constants;

public enum CacheComponents {
	AUTH("_auth"),
	APP_CORE("_app-core");
	
	public String value;
	
	private CacheComponents(String value) {
		this.value = value;
	}
}
