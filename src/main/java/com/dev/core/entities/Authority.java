package com.dev.core.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Authorization of Any entity.
 * @author p-kumar
 */
@Table(name = "AUTHORITIES")
@Entity
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Authority implements GrantedAuthority {
	private static final long serialVersionUID = 3832796845705458773L;

	@Id
	@JoinColumn(name = "users_id", unique = true)
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	@Setter private String id;
	
	@Column(name="authority") @Setter private String authority;
	@Column @Getter @Setter private String username;
	
	@Override
	public String getAuthority() {
		return authority;
	}

}
